# NabilaOlivia binar rental car 
website ini dibuat dengan mengunakan HTML,Java script(owl carrousel) dan juga dengan css. website ini dibuat dengan respoonsive yang bisa diakses diberbagai perangkat.
## file 
file didalam project terdiri dari 
- index.html bersisi tentang sorce code yang dapat diakases dan ditampilkan diinternet melalui browser internet.
- style.css bersisi sorcecode untuk mengatur tampilan yang menarik yang tertulis di index.html.
- img berisi public dan gambar, public sendiri berisi tentang Owl Carousel yang digunakan untuk membuat slidebar agar lebih menarik. dan gambar berisi semua gambar yang digunakan didalam website.
## Framework
project ini mengunakan beberapa framework yang terdiri dari
1. Boostrap
boostrap yang saya gunakan boostrap versi 4 dan 5. boostrap 4 digunakan untuk navigator dan class yang ada di html. boostrap 5 digunakan untuk off canvas dan accordion.
2. Owl Craousel
Owl Craousel digunakan untuk membuat slider lebih menarik.

## Cara menjalankan project 
1. Aktifkan aplikasi Visual Studi code di komputer atau laptop. Tunggu beberapa saat sampai tampilan aplikasi tersebut muncul.
2. Klik file – Open folder. Pilihlah file data yang berisi folder yang ingin dijalankan lalu klik open.
3. Klik Run   – Pilih Launch in (nama aplikasi web browser). tunggu beberapa saat sampai file terbuka.
4. jikaingin melihat apakah web tersebut bisa dijalankan disemua prangkat klik kanan pada mouse lalu klik inspect.

